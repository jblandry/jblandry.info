module.exports = function(grunt) {

	var

		// Project configuration.
		tasks = {
			default: ['core_css']
		},

		config = {
			pkg: grunt.file.readJSON('package.json'),

			// cleaner
			clean: {
				builds_tmp: {
					src: ['../webroot/assets/builds/*.tmp.*'],
					options: {
						force: true
					}
				}
			},

			// watcher
			watch: {
				all: {
					files: ['gruntfile.js', 'package.json'],
					tasks: 'default'
				},
			}
		}
	;

	// --------------------------------
	// CSS
	// --------------------------------
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-cssmin');


	// less
	config.less = {
		core: { files: { '../webroot/assets/builds/core-less.tmp.css': 'css/core.less' } }
	};


	// css
	config.cssmin = {
		core: { files: { '../webroot/assets/builds/core.css': [
			'css/libs/reset.css',
			'css/libs/normalize.css',
			'css/libs/html5boilerplate.css',
			'../webroot/assets/builds/core-less.tmp.css'
		]}}
	};


	// task
	tasks.core_css = [
		'less:core',
		'cssmin:core',
		'clean:builds_tmp'
	];

	config.watch.core_css = {
		files: [
			'css/**/*.css',
			'css/**/*.less'
		],
		tasks: 'core_css'
	};




	// --------------------------------
	// GRUNT
	// --------------------------------
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');

	grunt.initConfig(config);

	// tasks
	for (var name in tasks) {
		grunt.registerTask(name, tasks[name]);
	}
};

